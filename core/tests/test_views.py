from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from ..models import Department, Employee
from ..serializers import DepartmentSerializer, EmployeeSerializer


class EmployeeAPITest(APITestCase):
    def setUp(self):
        self.end_point = '/employee/'
        self.client = APIClient()
        mommy.make(Employee, 4)

    def test_list(self):
        response = self.client.get(self.end_point)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        employees = Employee.objects.all()
        serializer = EmployeeSerializer(employees, many=True)
        self.assertEquals(response.data, serializer.data)

    def test_create(self):
        employee_to_create = mommy.prepare(Employee, _save_related=True)
        serializer = EmployeeSerializer(employee_to_create, many=False)
        response = self.client.post(self.end_point, data=serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        employee = Employee.objects.get(email=employee_to_create.email)
        serializer = EmployeeSerializer(employee)
        self.assertEquals(response.data, serializer.data)

    def test_retrieve(self):
        employee = Employee.objects.first()
        response = self.client.get(f'{self.end_point}{employee.pk}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        serializer = EmployeeSerializer(employee)
        self.assertEquals(response.data, serializer.data)

    def test_delete(self):
        employee = Employee.objects.first()
        response = self.client.delete(f'{self.end_point}{employee.pk}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(employee not in Employee.objects.all())

    def test_update(self):
        employee = Employee.objects.first()
        pk = employee.pk
        updated = mommy.prepare(Employee, _save_related=True)
        serializer = EmployeeSerializer(updated)
        response = self.client.put(
            f'{self.end_point}{pk}/', data=serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        employee = Employee.objects.get(pk=pk)
        serializer = EmployeeSerializer(employee)
        self.assertEquals(response.data, serializer.data)

    def test_partial_update(self):
        employee = Employee.objects.first()
        pk = employee.pk
        response = self.client.patch(
            f'{self.end_point}{pk}/', data={'name': 'Adalberto Navarrete'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        employee = Employee.objects.get(pk=pk)
        serializer = EmployeeSerializer(employee)
        self.assertEquals(response.data, serializer.data)


class DepartmentAPITest(APITestCase):
    def setUp(self):
        self.end_point = '/department/'
        self.client = APIClient()
        mommy.make(Department, 4)

    def test_list(self):
        response = self.client.get(self.end_point)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        departments = Department.objects.all()
        serializer = DepartmentSerializer(departments, many=True)
        self.assertEquals(response.data, serializer.data)

    def test_create(self):
        department_to_create = mommy.prepare(Department, )
        serializer = DepartmentSerializer(department_to_create, many=False)
        response = self.client.post(self.end_point, data=serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        department = Department.objects.get(name=department_to_create.name)
        serializer = DepartmentSerializer(department)
        self.assertEquals(response.data, serializer.data)

    def test_retrieve(self):
        department = Department.objects.first()
        response = self.client.get(f'{self.end_point}{department.pk}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        serializer = DepartmentSerializer(department)
        self.assertEquals(response.data, serializer.data)

    def test_delete(self):
        department = Department.objects.first()
        response = self.client.delete(f'{self.end_point}{department.pk}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(department not in Department.objects.all())

    def test_update(self):
        department = Department.objects.first()
        pk = department.pk
        updated = mommy.prepare(Department)
        serializer = DepartmentSerializer(updated)
        response = self.client.put(
            f'{self.end_point}{pk}/', data=serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        department = Department.objects.get(pk=pk)
        serializer = DepartmentSerializer(department)
        self.assertEquals(response.data, serializer.data)

    def test_partial_update(self):
        department = Department.objects.first()
        pk = department.pk
        response = self.client.patch(
            f'{self.end_point}{pk}/', data={'name': 'Adalberto Navarrete'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        department = Department.objects.get(pk=pk)
        serializer = DepartmentSerializer(department)
        self.assertEquals(response.data, serializer.data)
