from django.test import TestCase
from model_mommy import mommy

from ..models import Department, Employee


class EmployeeTest(TestCase):
    def test_create_employee(self):
        employee = mommy.make(Employee)
        self.assertTrue(isinstance(employee, Employee))
        self.assertTrue(isinstance(employee.department, Department))
        self.assertEqual(employee.__str__(), employee.name)


class DepartmentTest(TestCase):
    def test_create_department(self):
        department = mommy.make(Department)
        self.assertEqual(department.__str__(), department.name)
