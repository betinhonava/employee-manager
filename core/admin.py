from django.contrib import admin
from .models import Employee, Department


# Register your models here.
@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'department']
    list_filter = ['department']


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    pass


admin.site.site_title = 'LuizaLabs Employee Manager'
admin.site.site_header = 'LuizaLabs Employee Manager Admin'