from django.db import models


class Employee(models.Model):
    name = models.CharField(max_length=255,)
    email = models.EmailField(unique=True,)
    department = models.ForeignKey('Department', models.CASCADE, related_name='employees',)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name
