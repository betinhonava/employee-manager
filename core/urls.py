from django.urls import path
from rest_framework.documentation import include_docs_urls
from . import views

urlpatterns = [
    path(
        'api/',
        include_docs_urls(title='LuizaLabs Employee Manager API Docs')),
    path('employee/', views.EmployeeList.as_view()),
    path('employee/<pk>/', views.EmployeeDetail.as_view()),
    path('department/', views.DepartmentList.as_view()),
    path('department/<pk>/', views.DepartmentDetail.as_view()),
]
